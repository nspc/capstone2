//USER SCHEMA

//Modules and Dependencies
const mongoose = require("mongoose");

//Schema/Blueprint
const userSchema = new mongoose.Schema({
  firstName:{
    type: String,
    required: [true, "First Name is required"],
  },
  lastName:{
    type: String,
    required: [true, "Last Name is required"],
  },
  mobileNo:{
    type:String,
    required:[true, "Mobile number is required"],
  },
  
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"], 
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
});

//Model
module.exports = mongoose.model("User", userSchema);