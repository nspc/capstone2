// Order Routes

// Dependencies and Modules
const express = require("express");

const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const productController = require("../controllers/product");

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// Create Order

router.post("/new", verify, orderController.createOrder);

//STRETCH GOAL

// Retrieve all orders (admin)
router.get("/all", verify, verifyAdmin, orderController.getAllOrders);

// Add to Cart 
router.post("/add/:productId", verify, orderController.addOrder);

module.exports = router;
