//Product Routes

//Dependencies and Modules
const express = require("express");

const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const productController = require("../controllers/product");

const auth = require("../auth") 
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();

//Create Product 
router.post("/add", verify, verifyAdmin, productController.addProduct);

//Get all Products
router.get("/all",verify, verifyAdmin, productController.getAllProducts);

//Get All Active Products
router.get("/",productController.getAllActiveProducts);

//Get a Specific Product
router.get("/:productId",productController.getProduct);

//Route for Search Products by Name
router.post('/searchByName', productController.searchProductsByName);	

//Search Products By Price Range
router.post('/searchByPrice', productController.searchProductsByPriceRange);


//Update Product information
router.put("/:productId",verify, verifyAdmin, productController.updateProduct);

//Archive a Product
router.put("/:productId/archive",verify, verifyAdmin, productController.archivedProduct);

//Activate a Product
router.put("/:productId/activate",verify, verifyAdmin, productController.activatedProduct);


//Export Route System
module.exports = router;