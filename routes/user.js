// User Routes

// Dependencies and Modules
const express = require("express");

const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const productController = require("../controllers/product");

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// Register a user
router.post("/register", async (req, res) => {
  try {
    const result = await userController.registerUser(req.body);
    if (result) {
      res.status(201).json({ message: "User registered successfully" });
    } else {
      res.status(500).json({ message: "User registration failed" });
    }
  } catch (error) {
    res.status(500).json({ message: "An error occurred", error });
  }
});

// User Authentication
router.post('/login', async (req, res) => {
  try {
    await userController.loginUser(req, res);
  } catch (error) {
    res.status(500).json({ message: "An error occurred", error });
  }
});



// Retrieve User Details
router.get('/details', verify, async (req, res) => {
  try {
    await userController.getProfile(req, res);
  } catch (error) {
    res.status(500).json({ message: "An error occurred", error });
  }
});


// Update user as an admin
router.put('/updateAdmin', verify, verifyAdmin, async (req, res) => {
  try {
    await userController.updateUserAsAdmin(req, res);
  } catch (error) {
    res.status(500).json({ message: "An error occurred", error });
  }
});

// Retrieve authenticated user's order
router.get("/orders", verify , async (req, res) => {
  try {
    await userController.getOrders(req, res);
  } catch (error) {
    res.status(500).json({ message: "An error occurred", error });
  }
});

//[SECTION] Reset Password
  router.put('/reset-password', verify, userController.resetPassword);

//[SECTION] Update Profile  
  router.put('/profile', verify, userController.updateProfile);


// Export Route System
module.exports = router;
