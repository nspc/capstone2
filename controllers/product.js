//Product Controller

//controller
const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order")

//Create a product
module.exports.addProduct = (req, res) => {
    const newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    });

    newProduct.save()
        .then(product => {
            res.send(true);
        })
        .catch(error => {
            console.error("Error creating product:", error);
            res.send(false);
        });
};

//Retrieve all products
module.exports.getAllProducts = (req, res) => {
    Product.find({})
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            console.error("Error retrieving products:", error);
            res.status(500).json({ message: "An error occurred", error });
        });
};

//Retrieve all active products
module.exports.getAllActiveProducts = (req, res) => {
    Product.find({ isActive: true })
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            console.error("Error retrieving active products:", error);
            res.status(500).json({ message: "An error occurred", error });
        });
};

//Retrieve a specific product
module.exports.getProduct = (req, res) => {
    Product.findById(req.params.productId)
        .then(result => {
            if (!result) {
                res.status(404).json({ message: "Product not found" });
            } else {
                res.send(result);
            }
        })
        .catch(error => {
            console.error("Error retrieving product:", error);
            res.status(500).json({ message: "An error occurred", error });
        });
};

// Update product information
    module.exports.updateProduct = (req, res) => {
            // Specify the fields/properties of the document to be updated
            let updatedProduct = {
                name : req.body.name,
                description : req.body.description,
                price : req.body.price
            };

            return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {

       
                if (error) {
                    return res.send(false);

      
                } else {                
                    return res.send(true);
                }
            })
            .catch(err => res.send(err))
        };


//Archive a product
module.exports.archivedProduct = (req, res) => {
    const archivedProduct = {
        isActive: false
    };

    Product.findByIdAndUpdate(req.params.productId, archivedProduct)
        .then(() => {
            res.send(true);
        })
        .catch(error => {
            console.error("Error archiving product:", error);
            res.send(false);
        });
};

// Activate a product
module.exports.activatedProduct = (req, res) => {
    const activatedProduct = {
        isActive: true
    };

    Product.findByIdAndUpdate(req.params.productId, activatedProduct)
        .then(() => {
            res.send(true);
        })
        .catch(error => {
            console.error("Error activating product:", error);
            res.send(false);
        });
};


//Search Product by Price Ramge

    exports.searchProductsByPriceRange = async (req, res) => {
        try {
          const { minPrice, maxPrice } = req.body;
      
          // Find products within the price range
          const products = await Product.find({
            price: { $gte: minPrice, $lte: maxPrice }
          });
      
          res.status(200).json({ products });
        } catch (error) {
          res.status(500).json({ error: 'An error occurred while searching for products' });
        }
      };



//Search Product by Name

module.exports.searchProductsByName = async (req, res) => {
    try {
      const { productName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const products = await Product.find({
        name: { $regex: productName, $options: 'i' }
      });
  
      res.json(products);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

