// Order Controller

// Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Create Order
module.exports.createOrder = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId, quantity } = req.body;

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    const totalAmount = product.price * quantity;

    const newOrder = new Order({
      userId: userId,
      products: [
        {
          productId: productId,
          quantity: quantity,
        },
      ],
      totalAmount: totalAmount,
    });

    const savedOrder = await newOrder.save();
    res.status(201).json({ message: "Order created successfully", order: savedOrder });
  } catch (error) {
    console.error("Error creating order:", error);
    res.status(500).json({ message: "An error occurred", error });
  }
};

// Retrieve all orders (admin)
module.exports.getAllOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: "Access forbidden" });
    }

    const orders = await Order.find()
      .populate({
        path: "userId",
        select: "-password",
      })
      .populate("products.productId");

    res.status(200).json({ orders });
  } catch (error) {
    console.error("Error retrieving orders:", error);
    res.status(500).json({ message: "An error occurred", error });
  }
};

// Add to Cart
module.exports.addOrder = async (req, res) => {
  try {
    const userId = req.user.id;
    const productId = req.params.productId;

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ message: "Product not found" });
    }

    const quantity = 1;
    const totalAmount = product.price * quantity;

    const order = new Order({
      userId,
      products: [
        {
          productId,
          quantity,
        },
      ],
      totalAmount,
    });

    await order.save();
    res.status(200).json({ message: "Product Added to Cart" });
  } catch (error) {
    console.error("Error adding product to cart:", error);
    res.status(500).json({ message: "An error occurred", error });
  }
};


