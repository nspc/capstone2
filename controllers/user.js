//User Controller

// Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration
module.exports.registerUser = (reqBody) => {

    // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newUser = new User({
      firstName : reqBody.firstName,
      lastName : reqBody.lastName,
      email : reqBody.email,
      mobileNo : reqBody.mobileNo,
      password : bcrypt.hashSync(reqBody.password, 10)
    })

    // Saves the created object to our database
    return newUser.save().then((user, error) => {

      // User registration failed
      if (error) {
        return false;

      // User registration successful
      } else {
        return true;
      }
    })
    .catch(err => err)
  };


//User Authenticatio
  module.exports.loginUser = (req, res) => {
    User.findOne({ email : req.body.email} ).then(result => {

      console.log(result);

      // User does not exist
      if(result == null){

        return res.send(false);

      // User exists
      } else {
        
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
        // If the passwords match/result of the above code is true
        if (isPasswordCorrect) {
  
          return res.send({ access : auth.createAccessToken(result) })

        // Passwords do not match
        } else {

          return res.send(false);
        }
      }
    })
    .catch(err => res.send(err))
  };



//Retrieve User Details
module.exports.getProfile = (req, res) => {
  User.findById(req.user.id)
    .then(result => {
      if (!result) {
        return res.status(404).json({ message: "User not found" });
      }
      
      const user = {
        email: result.email
      };

      return res.json(result);
    })
    .catch(err => {
      console.error("Error retrieving user profile:", err);
      res.status(500).json({ message: "An error occurred", error: err });
    });
};

//STRETCH GOAL

// Set User as Admin (admin)
module.exports.updateUserAsAdmin = async (req, res) => {
  try {
    const { userId } = req.body;
  
    if (!userId) {
      return res.status(400).json({ message: 'User ID is required in the request body.' });
    }
  
    const authenticatedUser = req.user; // Assuming authentication middleware is used
  
    // Check if authenticated user is an admin
    if (!authenticatedUser.isAdmin) {
      return res.status(403).json({ message: 'Access denied. Not an admin.' });
    }
  
    const userToUpdate = await User.findById(userId);
  
    if (!userToUpdate) {
      return res.status(404).json({ message: 'User not found.' });
    }
  
    // Perform the admin update logic here, e.g. updating user's role to 'admin'
    userToUpdate.isAdmin = true;
    await userToUpdate.save();
  
    return res.json({ message: 'User updated successfully.' });
  } catch (error) {
    console.error("Error updating user as admin:", error);
    return res.status(500).json({ message: 'An error occurred while updating the user.' });
  }
};

// Retrieve authenticated user's orders
module.exports.getOrders = async (req, res) => {
  try {
    const userId = req.user.id; 

    const userOrders = await Order.find({ userId: userId });

    res.status(200).json({ orders: userOrders });
  } catch (error) {
    console.error("Error retrieving orders:", error);
    res.status(500).json({ message: "An error occurred", error });
  }
};


  //[SECTION] Reset Password

  module.exports.resetPassword = async (req, res) => {
    try {


      const { newPassword } = req.body;
      const { id } = req.user; // Extracting user ID from the authorization header
    
      // Hashing the new password
      const hashedPassword = await bcrypt.hash(newPassword, 10);
    
      // Updating the user's password in the database
      await User.findByIdAndUpdate(id, { password: hashedPassword });
    
      // Sending a success response
      res.status(200).send({ message: 'Password reset successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Internal server error' });
    }
  };

  //[SECTION] Reset Profile
  module.exports.updateProfile = async (req, res) => {
    try {

      console.log(req.user);
      console.log(req.body);
      
    // Get the user ID from the authenticated token
      const userId = req.user.id;
    
      // Retrieve the updated profile information from the request body
      const { firstName, lastName, mobileNo } = req.body;
    
      // Update the user's profile in the database
      const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
      );
    
      res.send(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Failed to update profile' });
    }
    }
