## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: tobin@gmail.com
     - pwd: tobs1234
- Admin User:
    - email: nadia@gmail.com
    - pwd: patpee1234

    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/user/register
    - request body: 
        - email (string)
        - password (string)
- User authentication (POST)
	- http://localhost:4000/user/login
    - request body: 
        - email (string)
        - password (string)
- Create Product (Admin only) (POST)
	- http://localhost:4000/product/add
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/product/all
    - request body: none
- Retrieve all active products (GET)
	- http://localhost:4000/product/active
    - request body: none
- Retrieve single products (GET)
	- http://localhost:4000/product/:productid
    - request body: none
- Update Product Information(Admin only) (PUT)
	- http://localhost:4000/product/:productid
   	- request body: 
        - name (string)
        - description (string)
        - price (number)
- Archive Product (Admin only) (PUT)
	- http://localhost:4000/product/:productid/archive
    - request body: none
- Activate Product (Admin only) (PUT)
	- http://localhost:4000/product/:productid/activate
    - request body: none
- Non-Admin User Checkout or Create Order (POST)
	- http://localhost:4000/order/create
    - request body: 
    	- productId (string)
    	- quantity (number)
- Retrieve user details (GET)
	- http://localhost:4000/user/details
    - request body: 
    	- userId (string)
- Set User as Admin (Admin only) (PUT)
	- http://localhost:4000/user/updateAdmin
    - request body: 
    	- userId (string)
- Retrieve authenticated user's orders (GET)
	- http://localhost:4000/user/orders
    - request body: 
    	- userId (string)
- Retrieve all orders (Admin only) (GET)
	- http://localhost:4000/order/all
    - request body: none
- Add to Cart
	- Added Products 
		- http://localhost:4000/order/add/:productId
    	- request body: none


