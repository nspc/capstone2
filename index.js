// Capstone 2 - Ecommerce API

// Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

// Environment Setup
const port = process.env.PORT || 4000;

// Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//Database connection
	//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://natasshapaulinecua:L2yKQcWBm1R2PwFk@cluster0.svmfj0s.mongodb.net/Capstone2?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Connected to MongoDB Atlas.'));

// Backend Routes
app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);

// Server Gateway Response
if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
  });
}

module.exports = { app, mongoose };
